import numpy as np
from netCDF4 import Dataset
import os

def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return array[idx]


# BOX DEFINITIONS
label_all = ["INUP", "INAS", "INMID", "INWGH", "INNW", "INBMY"]
lat_N = [30, 29, 25, 32, 24]
lat_S = [26, 25, 17, 28, 20]
lon_W = [76, 89, 77, 74, 90]
lon_E = [80, 93, 85, 78, 94]

startyear = 2000
stopyear = 2004

input_path          = '../../monsoon_model_2017/_data/_SAT_daily.ppt/'
output_parent_path  = '../model/_obs/'

for year in range(startyear, stopyear+1):

    # INPUT FILE
    file_nc = input_path + 'PPT_3B42.Daily.' + str(year) + '.nc'
    file_input =  file_nc
    var_input = 'ppt'

    # DATA EXTRACTION
    data = Dataset(file_input, 'r')

    lon = data.variables['longitude'][:]
    lat = data.variables['latitude'][:]

    progress = 'processing year ' + str(year)
    print (progress)

    output_yearly_files = []

    for i_label in range(0, len(label_all)):

        output_path = output_parent_path + label_all[i_label] + '/ppt_TRMM_boundary/'
        output_file = output_path + file_nc.split('/')[-1][:-3] + '.dat'

        if not os.path.exists(output_path):
            os.makedirs(output_path)

        # for [i_label] in range(2, 4):

        id_lat_N = np.where(lat == find_nearest(lat, lat_N[i_label]))[0][0]
        id_lat_S = np.where(lat == find_nearest(lat, lat_S[i_label]))[0][0]
        id_lon_W = np.where(lon == find_nearest(lon, lon_W[i_label]))[0][0]
        id_lon_E = np.where(lon == find_nearest(lon, lon_E[i_label]))[0][0]

        id_lat_min = min(id_lat_N, id_lat_S)
        id_lat_max = max(id_lat_N, id_lat_S)

        id_lon_min = min(id_lon_W, id_lon_E)
        id_lon_max = max(id_lon_W, id_lon_E)

        if id_lat_N == id_lat_S:
            var_data = data.variables[var_input][:, id_lon_min:id_lon_max, id_lat_min]
        elif id_lon_W == id_lon_E:
            var_data = data.variables[var_input][:, id_lon_min, id_lat_min:id_lat_max]
        else:
            var_data = data.variables[var_input][:, id_lon_min:id_lon_max, id_lat_min:id_lat_max]


        with open(output_file, 'w+') as out_file:
            for i in range(len(var_data)):
                if id_lat_N == id_lat_S or id_lon_W == id_lon_E:
                    var_mean = np.mean(var_data[i, :])
                    var_mean = var_mean
                else:
                    var_mean = np.mean(var_data[i, :, :])
                    var_mean = var_mean
                    # out_string = str(i)
                    # out_string += '\t' + str(var_mean) + '\n'
                out_string = str(var_mean) + '\n'
                out_file.write(out_string)

        print ('written ', output_file)
        output_yearly_files.append(output_file)

        out_file.close()

    data.close()

    # contatenate all the yearly files together in their appropriate label/region
    print ('all yearly files: ')
    print (output_yearly_files)

    for i_label in range(0, len(label_all)):
    	cat_cmd = 'cat ' + output_yearly_files[i_label][:-8] + '*.dat >' + output_yearly_files[i_label][:-23] + 'PPT_' + str(startyear) + '-' + str(stopyear) + '.dat'
    	os.system(cat_cmd)
    	print (cat_cmd)

print ('done')


