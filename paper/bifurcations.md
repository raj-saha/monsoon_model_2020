# Notes on Bifurcations in simplified Zickfeld model

### July 1, 2020



__Methods__

I start with the simplified Zickfeld model from 2017. There are three boundary conditions that are fed into the model as time-varying inputs, obtained from reanalysis data. These are temperatures, sp. humidity, and wind-speed, all at the ground level. A fourth forcing is used, that is the time-varying daily average insolation at the center of the simulation region.

Instead of using time-varying boundary conditions and insolation forcing, I fix their values in time and examine the equilibrium response of the model. Then, I run the model repeatedly with a single boundary value *slowly* changing - i.e., allowing the model to come to equilibruim before changing the boundary value by a small amount and restarting the simulation, all other boundary values and inputs kept fixed. In each simulation with a specific boundary value I record the equilibrium Ta, qa, P, and w (I could record more, but these are the ones I started with) where the system has an equilibrium state, and max and min values where the system shows oscillations. Then I plot the equilibrium values against the slowly changing boundary values.

Below are bifurcation diagrams for the four sets of slowly varying inputs.

<div style="page-break-after: always; break-after: page;"></div>

### Slowly changing Temperature at the boundary

![all_plots_bif](/Users/raj/Documents/research/monsoons/monsoon_model_2020/model/_sensitivity_exps/_outputs/bif_T/all_plots_bif.png)



<div style="page-break-after: always; break-after: page;"></div>

### Slowly changing u at the boundary

![all_plots_bif](/Users/raj/Documents/research/monsoons/monsoon_model_2020/model/_sensitivity_exps/_outputs/bif_u/all_plots_bif.png)

<div style="page-break-after: always; break-after: page;"></div>

### Slowly changing insolation

![all_plots_bif](/Users/raj/Documents/research/monsoons/monsoon_model_2020/model/_sensitivity_exps/_outputs/bif_insol/all_plots_bif.png)



<div style="page-break-after: always; break-after: page;"></div>

### Slowly changing Sp. Humidity at the boundary

![all_plots_bif](/Users/raj/Documents/research/monsoons/monsoon_model_2020/model/_sensitivity_exps/_outputs/bif_q/all_plots_bif.png)