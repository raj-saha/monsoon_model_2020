# parameters

import numpy as np

sec_day    = 3600*24


# ...............................................................GEOMETRY
REarth    = 6370e3      # radius of earth [m]

# .....................................................PHYSICAL CONSTANTS
T0        = 273         # reference temperature [K]
Ts        = T0 + 17     # reference surface temperature [K]

g         = 9.8         # acc due to gravity [m s^-2]
R         = 287         # gas constant [J kg^-1 K-1]=[m^2 s^-2 K^-1]
Gamma_a   = 9.8e-3
Gamma_0   = 6e-3
Gamma     = Gamma_0 - Gamma_a      
Gamma_dew = 1.8e-3      # adiabatic lapse rate [K m^-1]

gamma0 	  = 6e-3		# [K m^-1]
gamma1 	  = 5.5e-5 		# [m^-1]
gamma2    = 1e-3		# [K m^-1]
aq        = 1e3			# [1]

H0        = R*T0/g      # scale height [m]
He        = 2e3         # water vapor scale height [m]
H_mon     = 3e3         # monsoon overturning height [m]
H_trop    = 14e3        # monsoon overturning height [m]
H_tib     = 1.5e3       # height of the Tibetan plateau [m]
H_Had     = 10e3        # height of Hadley overturning [m]
H_g       = 4           # thickness of ground [m]

Rd        = 287.058		# specific gas constant for dry air [J/(kg. K)]
Rw        = 461 		# specific gas constant for wet air [J/(kg. K)]

rho0      = 1.29        # reference air density [kg m^-3]
rho_g     = 1.6e3       # density of dry soil [kg m^-3]
p0        = 101325      # std atm pressure [N m^-2]
qsat0     = 3.808e-3    # saturation speficic humidity [1]

L         = 2.5e6       # latent heat of evaporation [J kg^-1]
Cair      = 1e3         # sp heat cap of air at c P [J kg^-1 K^-1]
Cg        = 1.48e3      # sp heat cap of moist soil [J kg^-1 K-1]

f         = 2*4.7e-5    # f = 2 w sin(lat)
f         = 1e-4        # Coriolis parameter [s^-1]

# ..................................................INSOLATION PARAMETERS
# .................................................................BUDYKO
B0 = 226
B1 = 2.25
B2 = 48.42
B3 = 1.61

# (Zickfeld's values for Budyko constants for OLR
# (B0 = 231B1 = 2.1B2 = 59B3 = 0.65))

# ..................................................CLOUDINESS PARAMETERS
a1st = 0.8          
a2st = 0.1
a3st = 1e-3
a1cu = 1.5e-3
a2cu = 30e-3

# .....................................................DERIVED PARAMETERS
# ...........................................................monsoon wind
#k_u_mon     = g*p0*H_mon/(rho0*f*R*T0^2*width_side) 
k_u_mon   = np.exp(H_trop/H0)*(-1 + np.exp(H_mon/H0))/(np.exp(H_trop/H0) - np.exp(H_mon/H0))
# k_u_Had_12   = (exp(H_trop/H0)*(-1 + exp(H_Had/H0)))/(-exp(H_Had/H0) + exp(H_trop/H0))


# # ......................... BOX PARAMETERS
# # ang_width_deg = 4

# all_labels = ["INUP", "INAS", "INMID", "INWGH", "INNW", "INBMY"]
# lat_N = [30, 29, 25, 22, 32, 24]
# lat_S = [26, 25, 17, 10, 28, 20]
# lon_W = [76, 89, 77, 72, 74, 90]
# lon_E = [80, 93, 85, 78, 78, 94]

# i_label = all_labels.index(label)
# print (i_label)

# width_NS = abs(lat_N[i_label]-lat_S[i_label])
# width_EW = abs(lon_E[i_label]-lon_W[i_label])

# if width_NS==width_EW:
#     width = width_NS
#     print ('width = ', width)
# else:
#     print ('region not a square, please use square region only')
#     sys.exit()

# # width         = (ang_width_deg/360)*2 * np.pi * REarth  # width of box [m]
# A_g           = width**2
# m_g           = A_g*H_g*rho_g # mass of soil

# .......................................................................
# .....................................................TUNABLE PARAMETERS
a1st = 0.8          
a1st = a1st*4.5          

a2st = 0.1
a3st = 1e-3
a1cu = 1.5e-3
a2cu = 30e-3

Ncu0 = 1             # cumulus cloud parameter (guess)
 
alphast = 0.4        # albedos of st cu and clear sky (guesses)
alphacu = 0.2        
alphacs = 0.1        

tau_p   = 2e4        # ppt time constant [s^-1]
tau_p   = sec_day    # ppt time constant [s^-1]

k_e     = 0.002      # k^2/ln(zs/zb)^2 [1] # original value used

#
k_e     = 0.0001      # k^2/ln(zs/zb)^2 [1] # modified value

k_Roff  = 0.4        
f1      = 24         
f1      = 180         
f2      = 120
tau_w   = 17e4
tau_w   = sec_day