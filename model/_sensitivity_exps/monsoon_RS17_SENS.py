# Functions written by Raj Saha, 2017
# Modified and simplified from Zickfeld 2010

# !! READS IN CONSTANT BOUNDARY CONDITIONS .. !!


import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import os
import sys 
from params_SENS import *
# import params_bif
# u_side_const = params_bif.u_side_const


# .......................................................................
# .........................................................BOX PARAMETERS
# ang_width_deg = 4

def box_params():

    ang_width_deg = 4

    width         = (ang_width_deg/360)*2 * np.pi * REarth  # width of box [m]
    A_g           = width**2
    m_g           = A_g*H_g*rho_g # mass of soil

    return width, A_g, m_g

# .......................................................................
# .........................................................TIME VARIABLES
def time_params(dur_sim_yr, dt_day):
    # dur_sim_yr = 1              # duration of simulation in years
    # dt_day     = 1                   # integration time-step in days

    sec_day    = 3600*24             # seconds in a day
    sec_month  = sec_day*30          # seconds in a year
    sec_year   = sec_day*365         # seconds in a year
    dt         = dt_day * sec_day    # integration time-step in sec
    dur_sim    = dur_sim_yr*sec_year # duration of simulation in sec

    dur_sim_months = dur_sim_yr*sec_year/sec_month
    
    return dur_sim, dt


## get bifurcation parameter values passed on here
def get_bif_params(param_name, param_val):

    params_dict = {
					"u_side_const" : u_side_const,
					"T_side_const" : T_side_const,
					"q_side_const" : q_side_const,
					"insol_const"  : insol_const,
					"f1" 		   : f1,
					"k_Roff" 	   : k_Roff
					}

    params_dict[param_name] = param_val

    return params_dict
    
# ----------------------------------------------------- READ BOUNDARY INPUTS
# ----------------------------------------------- Temp, Sp. Hum, Wind, insol
# ---------------------------------------------------- for a given box label
# --------------------------------------------------------------------------   
def read_inputs(dur_sim_yr, bif_param, bif_val):
    
    params_dict   = get_bif_params(bif_param, bif_val)
    
    u_side_const  = params_dict['u_side_const']
    T_side_const  = params_dict['T_side_const']
    q_side_const  = params_dict['q_side_const']
    insol_const   = params_dict['insol_const']
    
	#     print ('param values: u, T, qs:', u_side_const, T_side_const, q_side_const)
    
    # keep array structures the same, 
    # just read in the constant values

    # get number of lines in one file
    # (they should all be the same)
    Ndays = int(np.ceil(dur_sim_yr * 365))

    T_side  = np.zeros((Ndays, 4))
    q_side = np.zeros((Ndays, 4))
    u_side  = np.zeros((Ndays, 4))

    T_side[:,0] = T_side_const
    T_side[:,1] = T_side_const
    T_side[:,2] = T_side_const
    T_side[:,3] = T_side_const

    q_side[:,0] = q_side_const
    q_side[:,1] = q_side_const
    q_side[:,2] = q_side_const
    q_side[:,3] = q_side_const

    u_side[:,0] = u_side_const
    u_side[:,1] = u_side_const
    u_side[:,2] = u_side_const
    u_side[:,3] = u_side_const

    # Flip sign of N and E Wind inputs
    # to account for sign convention and flux into the box
    ## should not apply if wind is all incoming or all outgoing from all sides.
    # u_side[:,0] = -u_side[:,0]
    # u_side[:,1] = -u_side[:,1]
    
    # read insoaltion
    insol = insol_const
    
    return T_side, q_side, u_side, insol

# # ----------------------------------------------------- READ BOUNDARY INPUTS
# # ------------------------------------------------------------- Dynamic wind
# # --------------------------------------------------------------------------   
# def read_inputs_all_static(dur_sim_yr, fixed_bcs):
    

#     T_side_const, u_side_const, q_side_const, insol_const = fixed_bcs[0], fixed_bcs[1], fixed_bcs[2], fixed_bcs[3]

    
#     # keep array structures the same, 
#     # just read in the constant values

#     # get number of lines in one file
#     # (they should all be the same)
#     Ndays = int(np.ceil(dur_sim_yr * 365))

#     T_side  = np.zeros((Ndays, 4))
#     q_side = np.zeros((Ndays, 4))
#     u_side  = np.zeros((Ndays, 4))

#     T_side[:,0] = T_side_const
#     T_side[:,1] = T_side_const
#     T_side[:,2] = T_side_const
#     T_side[:,3] = T_side_const

#     q_side[:,0] = q_side_const
#     q_side[:,1] = q_side_const
#     q_side[:,2] = q_side_const
#     q_side[:,3] = q_side_const

#     u_side[:,0] = u_side_const
#     u_side[:,1] = u_side_const
#     u_side[:,2] = u_side_const
#     u_side[:,3] = u_side_const



#     # Flip sign of N and E Wind inputs
#     # to account for sign convention and flux into the box
#     ## should not apply if wind is all incoming or all outgoing from all sides.
#     # u_side[:,0] = -u_side[:,0]
#     # u_side[:,1] = -u_side[:,1]
    
#     # read insoaltion
#     insol = insol_const
    
#     return T_side, q_side, u_side, insol



# -------------------------------------- Temperature as a function of height
def Tz(z, Ta, qa, Ncu):
    return (Ta - (gamma0 + gamma1*(Ta-T0)*(1-aq*qa**2) - gamma2*Ncu)*z)

# -------------------------------- Specific humidity as a function of height
def qz(z, qa):
    return (qa*np.exp(-z/He))

# ------------------------------------------ Density as a function of height
def rho(z):
    return (rho0*np.exp(-z/H0))

# ----------------------------------------- Pressure as a function of height
def p(z):
    return (p0*np.exp(-z/H0))

# ------------------------------------------------------ saturation humidity
def qsat(z, Ta, qa, Ncu):
    return (qsat0*(p0/p(z))*np.exp(-((0.622*L/R)*(1/Tz(z,Ta,qa,Ncu)-1/T0))))

# -------------------------------- RUN TIME FUNCTIONS, COMPUTED FOR EACH DAY
# ---------------------------------------------- vertical wind speed, u_vert
def u_vert(t_day, A_g, width, T_side, q_side, u_side):    
    return width * integrate.quad(rho,0,H_mon)[0] * np.sum(u_side[t_day,:])/(A_g*rho(np.mean([H_Had, H_mon])))

def adv_heat_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu):
    return (Cair * width * ( \
                            + integrate.quad(lambda z: u0_p[i]*rho(z)*Tz(z, T_side[t_day,i], qa, Ncu), 0, H_mon  )[0] \
                            - integrate.quad(lambda z: u1_p[i]*rho(z)*Tz(z, Ta, qa, Ncu), H_mon, H_trop)[0] \
                            - integrate.quad(lambda z: u0_m[i]*rho(z)*Tz(z, Ta, qa, Ncu), 0, H_mon  )[0] \
                            + integrate.quad(lambda z: u1_m[i]*rho(z)*Tz(z, T_side[t_day,i], qa, Ncu), H_mon, H_trop)[0] \
                          ))

# ------------------------------------------------------- MOISTURE ADVECTION
# --------------------------------------------------------------------------
def adv_q_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu):    
    return (width * ( \
                            + integrate.quad(lambda z: u0_p[i]*rho(z)*qz(z, q_side[t_day,i]), 0, H_mon  )[0] \
                            - integrate.quad(lambda z: u1_p[i]*rho(z)*qz(z, qa), H_mon, H_trop)[0] \
                            - integrate.quad(lambda z: u0_m[i]*rho(z)*qz(z, qa), 0, H_mon  )[0] \
                            + integrate.quad(lambda z: u1_m[i]*rho(z)*qz(z, q_side[t_day,i]), H_mon, H_trop)[0] \
                          ))

# ------------------------------------------------------------- CLOUD SCHEME
# --------------------------------------------------------------------------
def clouds(v, qa, Ta):
    # ------------------------------------------------------ cumulus clouds
    if (v>0):    # upward velocity
        Ncu = Ncu0 * np.tanh(v/a1cu) * np.tanh(qa/a2cu)
    else: # downward velocity
        Ncu = 0 

    # ------------------------------------------------------ stratus clouds
    Nst   = (qa/qsat(0,Ta,qa,Ncu))**(1.5) * (a1st+a2st*(1+np.tanh(v/a3st)))

    # --------------------------------------------------------- total cloud
    N    = 1 - (1-Ncu)*(1-Nst)

    # --------------------------------------------------------- clear sky
    Ncs  = 1 - N
    
    return Ncu, Nst, N, Ncs

# ------------------------------------------- EULER'S SCHEME TO SOLVE SYSTEM
# --------------------------------------------------------------------------
def solve_monsoon_ODEs_euler(dur_sim, dt, width, A_g, m_g, Ta, qa, w, w2, T_side, q_side, u_side, insol, switch_dyn_u, param_bif, param_val):

    params_dict   = get_bif_params(bif_param, bif_val)
    
    f1  		= params_dict['f1']
    k_Roff  	= params_dict['k_Roff']
    
    sec_day    = 3600*24             # seconds in a day
    sec_month  = sec_day*30          # seconds in a year
    sec_year   = sec_day*365 
    dur_sim_yr = dur_sim/sec_year
    dur_sim_months = dur_sim_yr*sec_year/sec_month
    
    Ta_arr       = []
    qa_arr       = []
    w_arr        = []
    P_mmpday_arr = []
    E_mmpday_arr = []

    ## //////////////////////////////////////////////////////////// TIME LOOP
    ## //////////////////////////////////////////////////////////////////////
    t = dt

    while (t<dur_sim):

        # current time
        t_day         = int(np.floor(t/sec_day))
        t_months      = int(np.floor(t/sec_month))
        t_day_of_year = int(np.remainder(np.ceil(t/sec_day),365)+1)

        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< READ INPUT VARIABLES 
        # INPUTS: Temperature, Sp Hum, Wind, and Insolation


#         # Wind inputs    
#         u_side[t_day,0] = -u_side[t_day,0]
#         u_side[t_day,1] = -u_side[t_day,1]
        if switch_dyn_u == 'True':
            u_side[t_day, :] = (sin_alpha0*g*p00*H_mon/(ro00*f*2*R*T00*T00*width/100))*(Ta-T_side[t_day, :])


        # Insolation input [W m^-2]
        Iin           = insol

        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        # --------------------------------------------------- vertical velocity 
        v             = u_vert(t_day, A_g, width, T_side, q_side, u_side)

        # --------------------------------------------------- clouds
        Ncu, Nst, N, Ncs = clouds(v, qa, Ta)

        # Ncudot
        if (t<=dt):
            Ncudot = 0
        else:
            Ncudot = -(Ncu_old - Ncu)/dt


        # -------------------------------------------------------------- albedo
        # (clouds have specified albedos)


        alpha = Nst*alphast + Ncu*alphacu + Ncs*alphacs


        # ======================= FLUXES DUE TO MONSOON, HADLEY AND TRADE WINDS
        # (height change of N side due to Tibet not included)

        # ------------------------------------------------------------- monsoon 
        # (positive values imply box-ward flow)

        # u0 = surface wind
        # u1 = top wind

        # UP/DOWN-STREAM flows
        u0_p = (1/2)*(abs(u_side[t_day,:]) + u_side[t_day,:])
        u0_m = (1/2)*(abs(u_side[t_day,:]) - u_side[t_day,:])
        u1_p = k_u_mon * u0_p
        u1_m = k_u_mon * u0_m

        # heat advection, summed from all sides
        adv_heat = sum(adv_heat_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu) for i in range(0, 4)) 

        # moisture advection, summed from all sides
        adv_q    = sum(adv_q_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu) for i in range(0, 4)) 


        # ================================================= CONDENSATION HEIGHT
        # From the intersection of dew point and dry lapse rates
        # qaREL = 0.263*p0*qa*(exp(17.67*(Ta-T0)/(Ta-29.65)))^(-1)

        qaREL = qa/qsat(0, Ta, qa, Ncu)
        Tadew = Ta - (100-qaREL)/5

        H_c   = max((Ta-Tadew)/(Gamma_a - Gamma_dew),0)

        # ======================================================= PRECIPITATION
        if (H_c<H_trop and H_c>=0):
            # Zickfeld
            P = max((N/tau_p) * integrate.quad(lambda z: (rho(z)*qz(z,qa)), H_c, H_trop)[0], 0)
            #      P = max((N/tau_p) * quad(@(z) (rho(z).*qz(z,qa)), 0, H_trop),0)
        else:
            P = 0

        # ========================================================= EVAPORATION
        # (from ground only, only positive)
        # total surface wind:
        # usfc = abs(sum(u_side))

        usfc = np.sqrt((u_side[t_day,:][0]+u_side[t_day,:][2])**2 + (u_side[t_day,:][1]+u_side[t_day,:][3])**2)

        E  = max(k_e * w * usfc * rho0 * (qsat(0,Ta,qa,Ncu)-qa), 0)

        # =========================================================== RADIATION
        Iout  = B0 + B1*(Ta-T0) - N*(B2 + B3*(Ta-T0))

        # ================================================= GOVERNING EQUATIONS
        # MOISTURE EQ
        qdot_L      = (E-P)/(((1-np.exp((-2*H_trop)/H0))*H0*rho0)/2)
        qdot_adv    = (adv_q/A_g)/(((1-np.exp((-2*H_trop)/H0))*H0*rho0)/2)

        qadot       = qdot_L + qdot_adv

        # HEAT EQ
        RHS_HEATEQ  = A_g * (Iin*(1 - alpha) - Iout + L*(P - E)) + adv_heat

        Tadot       = (Cg*np.exp(1)**(H0**(-1)*H_trop)*m_g+A_g*Cair*H0*((-1)+(-1)* \
            gamma1*(H0+H_trop)*((-1)+aq*qa**2)+np.exp(1)**(H0**(-1)*H_trop)*( \
            1+gamma1*H0*((-1)+aq*qa**2)))*rho0)**(-1)*(np.exp(1)**(H0**(-1) \
            *H_trop)*RHS_HEATEQ+(-1)*A_g*Cair*H0*(((-1)+np.exp(1)**(H0**(-1)*H_trop) \
            )*H0+(-1)*H_trop)*rho0*(gamma2*Ncudot+2*aq*gamma1*qa*qadot* \
            ((-1)*T0+Ta)))

        # GROUND WATER EQ
        Roff  = k_Roff * P * w

        # wdot  = ((P - E - Roff)/f1) - (w-w2)/tau_w
        wdot  = ((P - E - Roff)/f1)
        # w2dot = (f1/f2) * (w-w2)/tau_w
        w2dot = 0

        # put these in a function and return some diagnostic variables.
        # =================================================== EULER INTEGRATION
        Ta = Ta + Tadot*dt
        qa = min(max(qa + qadot*dt, 1e-10),1)
        w  = min(max(w + wdot*dt, 0),1)
        w2 = min(max(w2 + w2dot*dt,0),1)

        # ============================================================== OUTPUT
        if (np.remainder(t, sec_day)==0): ## write values once every day      
            P_mmpday = P * sec_day
            E_mmpday = E * sec_day

            P_mmpday_arr.append(P_mmpday)
            Ta_arr.append(Ta)
            qa_arr.append(qa)
            w_arr.append(w)

            # 1    2   3   4  5  6    7         8         9                     10 11
            # t_day, Ta, qa, w, v, H_c, E_mmpday, P_mmpday, qsat(0,Ta,qa,Ncu)-qa, N, Ncu)

        
        if (np.remainder(t,sec_month)==0):
            print ('\r', 'month: ', int(t/sec_month), '/', int(dur_sim_months), end='')

        # ====================================================== TIME INCREMENT
        t = t + dt
        Ncu_old = Ncu
    ## //////////////////////////////////////////////////////// END TIME LOOP
    ## //////////////////////////////////////////////////////////////////////

    Ta_arr = np.array(Ta_arr)
    qa_arr = np.array(qa_arr)
    w_arr  = np.array(w_arr)
    P_mmpday_arr = np.array(P_mmpday_arr)
    E_mmpday_arr = np.array(E_mmpday_arr)

    return Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr

# ---------------------------------------------------- PLOTTING ROUTINE
def plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, T_side, q_side, u_side, label_SENS):

    fig, axs = plt.subplots(2,2, figsize = (18,10))

    axs[0, 0].step(Ta_arr, 'k')
    axs[0, 0].set_title('Temperature')
    axs[0, 0].set(ylabel = 'K')

    axs[0, 1].step(qa_arr, 'purple')
    axs[0, 1].set_title('Specific Humidity')
    axs[0, 1].set(ylabel = '')
    
    axs[1, 0].step(w_arr, 'g')
    axs[1, 0].set_title('Ground Moisture')
    axs[1, 0].set(ylabel = '')

    axs[1, 1].step(P_mmpday_arr[1:], '-', linewidth=1)
    axs[1, 1].set_title('Precipitation ')
    axs[1, 1].set(ylabel = '[mm/day]')

    for ax in axs.flat:
        ax.set(xlabel='Days')

    fig.tight_layout(pad=3.0)
    fig_title = 'Model Outputs [' + label_SENS + ']'
    fig.suptitle(fig_title, fontsize=14)
    
    # Output 
    output_path = '_outputs/' + label_SENS
    output_plotfile = output_path + '/out_plot.pdf'
    
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    
    plt.savefig(output_plotfile)  
    print ('Plot file saved as', output_plotfile)


# ---------------------------------------------------- SAVE ARRAYS 
def save_arrays(Ta_arr, qa_arr, w_arr, label_SENS):

    # Output 
    output_path = '_outputs/' + label_SENS

    output_file_temp = output_path + '/out_temp.dat'
    output_file_shum = output_path + '/out_shum.dat'
    output_file_gwat = output_path + '/out_gwat.dat'

    np.savetxt(output_file_temp, Ta_arr)
    np.savetxt(output_file_shum, qa_arr)
    np.savetxt(output_file_gwat, w_arr)

    print ('output files: ', output_file_temp, output_file_shum, output_file_gwat)