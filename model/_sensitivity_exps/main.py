# MAIN SCRIPT TO RUN MONSOON SIMULATION
# Raj Saha, 2020

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import sys


# sensitivity label
label_SENS = 'const_BC_INSOL_1'
# label_SENS = 'const_BC_INSOL_2'

# .......................................................................
# SET TIME PARAMETERS
# .......................................................................
dur_sim_yr = 0.3              # duration of simulation in years
dt_day     = 1              # integration time-step in days


# IMPORT MODEL FUNCTIONS
# .......................................................................
# model parameters called from within the function file
from params_SENS import *
import monsoon_RS17_SENS as monRS17_SENS


# GET BOX PARAMETERS
width, A_g, m_g = monRS17_SENS.box_params()

# GET TIME VARIABLES
dur_sim, dt 	= monRS17_SENS.time_params(dur_sim_yr, dt_day)

# .......................................................................
# READ INPUTS
# .......................................................................
T_side, qs_side, u_side, insol = monRS17_SENS.read_inputs(dur_sim_yr)


# .......................................................................
## DEFINE INITIAL CONDITIONS
# .......................................................................
Ta = Ts
qa = 0.015
w  = 0.35        # soil moisture content [kg m^-2]
w2 = 0           # soil moisture content [kg m^-2]
# initial conditions to test if the system is a limit cycle -- to be run after above ICs

if label_SENS == 'const_BC_INSOL_2':
	Ta = 305
	qa = 0.05


print ('simulation length = ', dur_sim_yr, ' yr')
print ('time step = ', dt_day, ' days\n')
print ('-------------------------------')

print ('sim progress ...')

# .......................................................................
## CALL SYSTEM SOLVER
# .......................................................................
Ta_arr, qa_arr, w_arr, P_mmpday_arr = monRS17_SENS.solve_monsoon_ODEs_euler(dur_sim, dt, width, A_g, m_g, Ta, qa, w, w2, T_side, qs_side, u_side, insol)

print ('\ndone')

# .......................................................................
## PLOT ARRAYS
print ('Model outputs below\n')

monRS17_SENS.plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, T_side, qs_side, u_side, label_SENS)
monRS17_SENS.save_arrays(Ta_arr, qa_arr, w_arr, label_SENS)
