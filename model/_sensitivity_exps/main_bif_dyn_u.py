# MAIN SCRIPT TO RUN MONSOON SIMULATION
# Raj Saha, 2020

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import sys
import time
import os

# .......................................................................
# SET TIME PARAMETERS
# .......................................................................
dur_sim_yr = 2              # duration of simulation in years
dt_day     = 1              # integration time-step in days


# IMPORT MODEL FUNCTIONS
# .......................................................................
# model parameters called from within the function file
from params_SENS import *
import monsoon_RS17_SENS as monRS17_SENS


# GET BOX PARAMETERS
width, A_g, m_g = monRS17_SENS.box_params()

# dynamic wind parameter
DL = width*1.0

# GET TIME VARIABLES
dur_sim, dt 	= monRS17_SENS.time_params(dur_sim_yr, dt_day)

print ('simulation length = ', dur_sim_yr, ' yr')
print ('time step = ', dt_day, ' days\n')
print ('-------------------------------')


# SET BIFURCATION PARAMETER
# ..........................
# param_bif = 'u_side_const'
# param_bif = 'T_side_const'
param_bif = 'q_side_const'
# param_bif = 'insol_const'

# ..........................
print ('----------------------------------')
print ('Bifurcation parameter: ', param_bif)
print ('----------------------------------')


# .......................................................................
## DEFINE INITIAL CONDITIONS
# .......................................................................
Ta0 = Ts
qa0 = 0.015
w0  = 0.35
w20 = 0

Ta = Ts
qa = qa0
w  = w0        # soil moisture content [kg m^-2]
w2 = w20           # soil moisture content [kg m^-2]


fixed_bcs = T_side_const, u_side_const, q_side_const, insol_const

    
time.sleep(0.01)
print ('\n', param_bif, param_val)
# .......................................................................
# READ INPUTS & ASSIGN BIF PARAMETER VALUE(S)
# .......................................................................
T_side, q_side, u_side, insol = monRS17_SENS.read_inputs_all_static(dur_sim_yr, fixed_bcs)

print ('sim progress ...')

# .......................................................................
## CALL SYSTEM SOLVER
# .......................................................................
switch_dyn_u = 'True'
Ta_arr, qa_arr, w_arr, P_mmpday_arr = monRS17_SENS.solve_monsoon_ODEs_euler(dur_sim, dt, width, A_g, m_g, Ta, qa, w, w2, T_side, q_side, u_side, insol, switch_dyn_u)

    
# .......................................................................
# PLOT ARRAYS
print ('Model outputs below\n')
monRS17_SENS.plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, T_side, q_side, u_side, label_SENS)
monRS17_SENS.save_arrays(Ta_arr, qa_arr, w_arr, label_SENS)


print ('\ndone')