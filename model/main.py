# MAIN SCRIPT TO RUN MONSOON SIMULATION
# Raj Saha, 2020

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import sys


# get label
label = sys.argv[1]

# .......................................................................
# SET TIME PARAMETERS
# .......................................................................
dur_sim_yr = 1              # duration of simulation in years
dt_day     = 1              # integration time-step in days


# IMPORT MODEL FUNCTIONS
# .......................................................................
# model parameters called from within the function file
from params import *
import monsoon_RS17 as monRS17


# GET BOX PARAMETERS
width, A_g, m_g = monRS17.box_params(label)

# GET TIME VARIABLES
dur_sim, dt = monRS17.time_params(dur_sim_yr, dt_day)

# .......................................................................
# READ INPUTS
# .......................................................................
T_side, qs_side, u_side, insol = monRS17.read_inputs(label)


# .......................................................................
## DEFINE INITIAL CONDITIONS
# .......................................................................
Ta = Ts
qa = 0.015
w  = 0.35        # soil moisture content [kg m^-2]
w2 = 0           # soil moisture content [kg m^-2]


print ('simulation length = ', dur_sim_yr, ' yr')
print ('time step = ', dt_day, ' days\n')
print ('-------------------------------')
print ('running sim over box ', label, '\n')

print ('sim progress ...')

# .......................................................................
## CALL SYSTEM SOLVER
# .......................................................................
Ta_arr, qa_arr, w_arr, P_mmpday_arr = monRS17.solve_monsoon_ODEs_euler(dur_sim, dt, width, A_g, m_g, Ta, qa, w, w2, T_side, qs_side, u_side, insol)

print ('\ndone')

# .......................................................................
## PLOT ARRAYS
print ('Model outputs below')
print ('Plot lines in red show input values at the boundaries')
monRS17.plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, T_side, qs_side, u_side, label)

