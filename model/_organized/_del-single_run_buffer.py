# MAIN SCRIPT TO RUN MONSOON SIMULATION
# Raj Saha, 2020
# organized build

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import sys
import time
import os

# .......................................................................
# SET TIME PARAMETERS
# .......................................................................
dur_sim_yr = 2              # duration of simulation in years
dt_day     = 1              # integration time-step in days


# IMPORT MODEL FUNCTIONS
# .......................................................................
# model parameters called from within the function file
from params_SENS import *
import monsoon_RS17_SENS as monRS17_SENS


# GET BOX PARAMETERS
width, A_g, m_g = monRS17_SENS.box_params()

# dynamic wind parameter
DL = width*1.0

# GET TIME VARIABLES
dur_sim, dt 	= monRS17_SENS.time_params(dur_sim_yr, dt_day)

print ('simulation length = ', dur_sim_yr, ' yr')
print ('time step = ', dt_day, ' days\n')
print ('-------------------------------')


# SET BIFURCATION PARAMETER
# ..........................
# param_bif = 'u_side_const'
# param_bif = 'T_side_const'
# param_bif = 'q_side_const'
# param_bif = 'insol_const'
param_bif = 'f1'
# param_bif = 'k_Roff'

# ..........................
print ('----------------------------------')
print ('Bifurcation parameter: ', param_bif)
print ('----------------------------------')

# PARAM MIN/MAX
u_side_const_min   = -0.01
u_side_const_max   = 1.1

T_side_const_min   = 270
T_side_const_max   = 320

q_side_const_min   = 0.01
q_side_const_max   = 0.07

insol_const_min   = 350
insol_const_max   = 510

f1_min = 24
f1_max = 200

k_Roff_min = 0.2
k_Roff_max = 1.2

params_range_dict = {
                    "u_side_const" : [u_side_const_min, u_side_const_max],
                    "T_side_const" : [T_side_const_min, T_side_const_max],
                    "q_side_const" : [q_side_const_min, q_side_const_max],
                    "insol_const"  : [insol_const_min, insol_const_max],
                    "f1"           : [f1_min, f1_max],
                    "k_Roff"       : [f1_min, f1_max],
                    }

# initialization of equilibrium arrays
Ta_eq = []
qa_eq = []
P_eq  = []
E_eq  = []
w_eq  = []
param_val_arr = []

# ARRAY HOLDING PARAMETER VALUES IN SPECIFIED RANGE
N_param_vals = 100
param_range  = np.linspace(params_range_dict[param_bif][0], params_range_dict[param_bif][1], N_param_vals)


# .......................................................................
## DEFINE INITIAL CONDITIONS
# .......................................................................
Ta0 = Ts
qa0 = 0.015
w0  = 0.35
w20 = 0


Ta = Ts
qa = qa0
w  = w0        # soil moisture content [kg m^-2]
w2 = w20           # soil moisture content [kg m^-2]

# initial conditions to test if the system is a limit cycle -- to be run after above ICs
# Ta = 305
# qa = 0.05

## START PARAM LOOP -----------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
for i_param in range(0, len(param_range)):
    
    param_val = param_range[i_param]
    
    time.sleep(0.01)
    print ('\n', param_bif, param_val)
    # .......................................................................
    # READ INPUTS & ASSIGN BIF PARAMETER VALUE(S)
    # .......................................................................
    T_side, q_side, u_side, insol = monRS17_SENS.read_inputs(dur_sim_yr, param_bif, param_val)

    print ('sim progress ...')

    # check that ICs have not blown up
    if (i_param>0 and (abs(Ta_arr[-1])>1000 or abs(qa_arr[-1])>1000 or abs(P_mmpday_arr[-1])>1000)):
        print ('... encountered NaNs previously ... resetting initial conditions ...')
        Ta = Ta0
        qa = qa0
        w  = w0
        w2 = w20
    elif (i_param > 0):
        Ta = Ta_arr[-1]
        qa = qa_arr[-1]
        w  = w_arr[-1]
        w2 = w20

    # .......................................................................
    ## CALL SYSTEM SOLVER
    # .......................................................................

    Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr = monRS17_SENS.solve_monsoon_ODEs_euler(dur_sim, dt, width, A_g, m_g, Ta, qa, w, w2, T_side, q_side, u_side, insol)

    Ta_eq.append(Ta_arr[-100:-1])
    qa_eq.append(qa_arr[-100:-1])
    P_eq.append(P_mmpday_arr[-100:-1])
    w_eq.append(w_arr[-100:-1])
    param_val_arr.append(param_val)
    
    # .......................................................................
    ## PLOT ARRAYS
#     print ('Model outputs below\n')
#     monRS17_SENS.plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, T_side, q_side, u_side, label_SENS)
#     monRS17_SENS.save_arrays(Ta_arr, qa_arr, w_arr, label_SENS)


# .......................................................................
# .......................................................................
# SAVE EQUILIBRIUM VALUES WITH CHANGING BIF PARAMETER

# write the min and max values for corresponding bif param 
# (to capture oscillations)
Ta_eq_bifparam = []
for i in range(0, len(param_val_arr)):
    maxval = max(Ta_eq[i])
    minval = min(Ta_eq[i])
    Ta_eq_bifparam.append([param_val_arr[i], minval, maxval])
Ta_eq_bifparam = np.array(Ta_eq_bifparam)

qa_eq_bifparam = []
for i in range(0, len(param_val_arr)):
    maxval = max(qa_eq[i])
    minval = min(qa_eq[i])
    qa_eq_bifparam.append([param_val_arr[i], minval, maxval])
qa_eq_bifparam = np.array(qa_eq_bifparam)

P_eq_bifparam = []
for i in range(0, len(param_val_arr)):
    maxval = max(P_eq[i])
    minval = min(P_eq[i])
    P_eq_bifparam.append([param_val_arr[i], minval, maxval])
P_eq_bifparam = np.array(P_eq_bifparam)

E_eq_bifparam = []
for i in range(0, len(param_val_arr)):
    maxval = max(P_eq[i])
    minval = min(P_eq[i])
    P_eq_bifparam.append([param_val_arr[i], minval, maxval])
P_eq_bifparam = np.array(P_eq_bifparam)

w_eq_bifparam = []
for i in range(0, len(param_val_arr)):
    maxval = max(w_eq[i])
    minval = min(w_eq[i])
    w_eq_bifparam.append([param_val_arr[i], minval, maxval])
w_eq_bifparam = np.array(w_eq_bifparam)

path_output_eq_bifparam = '_outputs/bif_' + param_bif.split('_')[0] + '/'
if not os.path.exists(path_output_eq_bifparam):
    os.makedirs(path_output_eq_bifparam)

file_output_Ta_eq_bifparam = path_output_eq_bifparam + 'Ta_eq_bif.dat'
file_output_qa_eq_bifparam = path_output_eq_bifparam + 'qa_eq_bif.dat'
file_output_w_eq_bifparam  = path_output_eq_bifparam + 'w_eq_bif.dat'
file_output_P_eq_bifparam  = path_output_eq_bifparam + 'P_eq_bif.dat'

# write equilibrium values -- stuff to make bif plots out of
np.savetxt(file_output_Ta_eq_bifparam, Ta_eq_bifparam)
np.savetxt(file_output_qa_eq_bifparam, qa_eq_bifparam)
np.savetxt(file_output_w_eq_bifparam, w_eq_bifparam)
np.savetxt(file_output_P_eq_bifparam, P_eq_bifparam)


print ('\ndone')