# pip install PyYAML==5.1.2
import yaml
# from def_single_run import single_run
import numpy as np
import monsoon_RS2020 as monRS20
import os


def read_controls(file_control):    

    with open('controls.yaml') as file:
        controls = yaml.load(file, Loader=yaml.FullLoader)

    # set variables equal to dict values
    dur_sim_yr = controls['dur_sim_yr']
    dt_day = controls['dt_day']

    return controls

def get_str_val(x):

    if x<0.001:
        x_str = "{:.2e}".format(x)
    else:
        x_str = str(param_range[i_param_val])[:5]

#     x_str = "{:03d}".format(x)
        
    return x_str

def get_controls_single_run(param_bif, param_val, Ta_0, qa_0, w_0, w2_0, **controls):

    controls_single_run = {
        'dur_sim_yr'        : controls['dur_sim_yr'],
        'dt_day'            : controls['dt_day'],

        'veg_albedo'        : controls['ctl_run']['veg_albedo'],
        'veg_evap'          : controls['ctl_run']['veg_evap'],
        
        'dyn_Ta'            : controls['ctl_run']['dyn_Ta'],
        'dyn_qa'            : controls['ctl_run']['dyn_qa'],
        'dyn_v'             : controls['ctl_run']['dyn_v'],

        'bc_type'           : controls['ctl_bcs']['type'],
        'bc_region'         : controls['ctl_bcs']['region'],

        'param_bif'         : param_bif,
        'param_val'         : param_val,

        'initial_conditions' : [Ta_0, qa_0, w_0, w2_0]
    }
    
    return controls_single_run

def label_dynamic_or_prescribed(dyn_Ta, dyn_qa, dyn_v):

    type_label = '__pres_'

    if dyn_Ta == False:
        type_label = type_label + '_Ta'
        
    if dyn_qa == False:
        type_label = type_label + '_qa'
        
    if dyn_v == False:
        type_label = type_label + '_v'

    if dyn_Ta == True and dyn_qa == True and dyn_v == True:
        type_label = ''
        
    return type_label


def call_solver(output_folder, output_file_data_prefix, output_file_plot, output_plot_title, label, **controls_single_run):
    # .......................................................................
    # CALL SYSTEM SOLVER
    # .......................................................................
    Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr, T_side, q_side, u_side, params_dict = monRS20.solve_monsoon_ODEs_euler(**controls_single_run)

    # SAVE OUTPUT ARRAYS
    if out_data_all_sims == True:
        monRS20.save_arrays_params(Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr, output_folder, output_file_data_prefix, params_dict)

    # PLOT RESULTS    
    if out_plots_all_sims == True:
        monRS20.plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr, T_side, q_side, u_side, label, output_file_plot, output_plot_title, out_plots_single_show)
        
        
# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------

# MAIN PROGRAM

# read yaml fileb
controls = read_controls('controls.yaml')
# print (controls)

# notes ...
# bifparams are not important
# important controls:
    # ctl_run: veg_*, dyn_*
    # ctl_bcs: * type = grid?
    # output controls


dur_sim_yr            = controls['dur_sim_yr']
dt_day                = controls['dt_day']

veg_albedo            = controls['ctl_run']['veg_albedo']
veg_evap              = controls['ctl_run']['veg_evap']

grid_sim              = controls['ctl_run']['grid_sim']

dyn_Ta                = controls['ctl_run']['dyn_Ta']
dyn_qa                = controls['ctl_run']['dyn_qa']
dyn_v                 = controls['ctl_run']['dyn_v']

bc_type               = controls['ctl_bcs']['type']
bc_region             = controls['ctl_bcs']['region']

out_data_all_sims     = controls['ctl_out']['data_all_sims']
out_data_eqpts        = controls['ctl_out']['data_eqpts']
out_plots_all_sims    = controls['ctl_out']['plots_all_sims']
out_plots_eqpts       = controls['ctl_out']['plots_eqpts']
out_plots_single_show = controls['ctl_out']['plots_single_show']

# .......................................................................
## DEFINE INITIAL CONDITIONS
# .......................................................................
Ta_0 = 273+17
qa_0 = 0.015
w_0  = 0.35
w2_0 = 0

# single parameter name
# N_bif_params = len(controls['ctl_run']['bif_params'])

# check type label, ie if main variables are dynamic or prescribed
# type_label = label_dynamic_or_prescribed(dyn_Ta, dyn_qa, dyn_v)



if grid_sim == True:
    input_path = '_grid_sim/_inputs_03x03/_inputs/'




# code for bifurcation parameters
if N_bif_params == 0:
    param_bif = None
    param_val = None
    
    controls_single_run = get_controls_single_run(param_bif, param_val, Ta_0, qa_0, w_0, w2_0, **controls)
    
    label = bc_region + '_' + bc_type + '__defaults' + type_label
    output_plot_title = label

    output_folder           = '_outputs/' + label 
    output_file_plot        = output_folder + '/plot.pdf'
    output_file_data_prefix = output_folder + '/out_'

    call_solver(output_folder, output_file_data_prefix, output_file_plot, output_plot_title, label, **controls_single_run)

else:

    for i_param in range(0, N_bif_params):
        param_bif         = controls['ctl_run']['bif_params'][i_param][0]
        param_range       = np.linspace(controls['ctl_run']['bif_params'][i_param][1][0], 
                                        controls['ctl_run']['bif_params'][i_param][1][1], 
                                        controls['ctl_run']['bif_params'][i_param][1][2])
        
        label = bc_region + '_' + bc_type + '__' + param_bif + type_label
        output_folder    = '_outputs/' + label
        output_file_param_bif_values = output_folder + '/data/param_vals.dat'
        param_bif_values = []
        
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
                
        print ('......................................................')
        print ('Bifurcation parameter:', param_bif)
        print ('......................................................\n')

        for i_param_val in range(0, len(param_range)):
            param_val = param_range[i_param_val]
            i_param_val_str = "{:03d}".format(i_param_val)
            
            param_val_str = get_str_val(param_val) 
            param_bif_values.append(param_val)
            
            print (param_bif, '=', param_val)
            
#             label = label + ':' + param_val_str
            output_plot_title = label + ': ' + param_bif + '=' + param_val_str

            output_file_plot        = output_folder + '/plots/plot_' + i_param_val_str + '.pdf'       
            output_file_data_prefix = output_folder + '/data/out_' + i_param_val_str + '_'   
            
            controls_single_run = get_controls_single_run(param_bif, param_val, Ta_0, qa_0, w_0, w2_0, **controls)

            call_solver(output_folder, output_file_data_prefix, output_file_plot, output_plot_title, label, **controls_single_run)
            
            
            print ('\n')

        param_bif_values = np.array(param_bif_values)
        # write sequential parameter values corresponding to padded indices
        np.savetxt(output_file_param_bif_values, param_bif_values)
        print ('param values: ', output_file_param_bif_values)

print ('done')