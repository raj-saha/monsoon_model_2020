import yaml # pip install PyYAML==5.1.2
import numpy as np
from netCDF4 import Dataset
import os
import sys

def read_controls(file_control):    
    with open('controls.yaml') as file:
        controls = yaml.load(file, Loader=yaml.FullLoader)
    return controls

def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return array[idx]

def writefile(var_array, label, edge, folder):

    if edge_or_mid=='EDGE':
        file_output = folder + label + '_' + edge + '.dat'
        # print ('printing', file_output)

        with open(file_output, 'w+') as out_file:
            for i in range(len(var_array)):
                var_mean = np.mean(var_array[i, :])

                out_string = str(var_mean) + '\n'
                out_file.write(out_string)

        out_file.close()

    elif edge_or_mid=='MID':
        file_output = folder + label + '_mid.dat'
        # print ('printing', file_output)

        with open(file_output, 'w+') as out_file:
            for i in range(len(var_array)):
                var_mean = np.mean(var_array[i, :, :])

                out_string = str(var_mean) + '\n'
                out_file.write(out_string)

        out_file.close()


def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

def clear_folder(folder):
    cmd_rm = 'rm ' + folder + '*.dat'
    os.system(cmd_rm)


# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# START MAIN PROGRAM

# sample commands:
# %run read_T_q_u_v 'air' 'T' 'EDGE'
# %run read_T_q_u_v 'shum' 'q' 'EDGE'
# %run read_T_q_u_v 'uwnd' 'u' 'EDGE'
# %run read_T_q_u_v 'vwnd' 'u' 'EDGE'
# %run read_T_q_u_v 'omega' 'v' 'MID'
# %run read_T_q_u_v 'air' 'T' 'MID'
# %run read_T_q_u_v 'shum' 'q' 'MID'
# %run -i 'compute_dailyinsol.py'

# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------
# # BOX DEFINITIONS
# label_all = ["INUP", "INAS", "INMID", "INNW", "INBMY"]
# lat_N = [30, 29, 25, 32, 24]
# lat_S = [26, 25, 17, 28, 20]
# lon_W = [76, 89, 77, 74, 90]
# lon_E = [80, 93, 85, 78, 94]

# label_all = ["INAS", "INMID"]
# lat_N     = [29, 25]
# lat_S     = [25, 17]
# lon_W     = [89, 77]
# lon_E     = [93, 85]

label_all = ["05x05_05_05", "05x05_02_03"]
lat_N     = [10, 30]
lat_S     = [0,  20]
lon_W     = [50, 80]
lon_E     = [60, 90]

# start and stop years of RA data
startyear = 2000
stopyear  = 2000

# edge_or_mid = raw_input('[E]dge or [M]id values? ')
# edge_or_mid = 'EDGE'
# edge_or_mid = 'MID'


level_input = 0         # raw_input("Enter Level: ")
label_level = '_1000mb'

# read command line arguments
var_input   = sys.argv[1]
var_label   = sys.argv[2]
edge_or_mid = sys.argv[3]

if var_label == 'u':
    edge_or_mid = 'EDGE'
    
if var_label == 'v':
    level_input = 5
    label_level = '_600mb'
    edge_or_mid = 'MID'

print ('---------------------------------------')
print (' EXTRACT MODEL INPUTS FROM RE-ANALYSIS')
print ('----------------------------------------')



input_path         = '../../../../monsoon_model_2017/_data/_RA_daily.' + var_input +'/'


print ('var_input: ', var_input)
print ('var_label: ', var_label)
print ('edge or mid: ', edge_or_mid)
print ('level: ', level_input, label_level)

for year in range(startyear, stopyear+1):

    # INPUT FILE
    file_nc    = input_path + var_input + '.' + str(year) + '.nc'
    file_input = file_nc

    # DATA EXTRACTION
    data = Dataset(file_input, 'r')

    day = data.variables['time'][:]
    lon = data.variables['lon'][:]
    lat = data.variables['lat'][:]

    progress = 'processing year ' + str(year)
    print (progress)

    output_yearly_files = []

    for i_label in range(0, len(label_all)):

        label      = label_all[i_label]
    
        output_folder_name = '_inputs/' + label
        print ('output folder:', output_folder_name)
        
        output_path        = output_folder_name + '/_buffer/'
        output_path_inputs = output_folder_name
        create_folder(output_path)
        clear_folder(output_path)
        create_folder(output_path_inputs)

        label_year = label + '_' + str(year)
        
        id_lat_N = np.where(lat == find_nearest(lat, lat_N[i_label]))[0][0]
        id_lat_S = np.where(lat == find_nearest(lat, lat_S[i_label]))[0][0]
        id_lon_W = np.where(lon == find_nearest(lon, lon_W[i_label]))[0][0]
        id_lon_E = np.where(lon == find_nearest(lon, lon_E[i_label]))[0][0]

        if edge_or_mid=='EDGE':
            # extracting average value along four edges
            if var_input != 'uwnd':
                var_files_side_N = data.variables[var_input][:, level_input, id_lat_N, id_lon_W:id_lon_E]
                var_files_side_S = data.variables[var_input][:, level_input, id_lat_S, id_lon_W:id_lon_E]
                writefile(var_files_side_N, label_year, 'N', output_path)
                writefile(var_files_side_S, label_year, 'S', output_path)

            if var_input != 'vwnd':
                var_files_side_E = data.variables[var_input][:, level_input, id_lat_N:id_lat_S, id_lon_E]
                var_files_side_W = data.variables[var_input][:, level_input, id_lat_N:id_lat_S, id_lon_W]
                writefile(var_files_side_E, label_year, 'E', output_path)
                writefile(var_files_side_W, label_year, 'W', output_path)

        elif edge_or_mid=='MID':
            # extracting average value within the box
            var_files = data.variables[var_input][:, level_input, id_lat_N:id_lat_S, id_lon_W:id_lon_E]
            writefile(var_files, label_year, 'MID', output_path)

    data.close()
    
    # CONCATENATE ALL YEARLY FILES
    for i_label in range(0, len(label_all)):
        label = label_all[i_label]
        output_folder_name = '_inputs/' + label
        output_path        = output_folder_name + '/_buffer/'
        output_path_inputs = output_folder_name
        
#         label_year  = label_all[i_label] + str(year)
        
        catfile_pre = output_path_inputs + '/input_' + var_label + '_'
        print (catfile_pre)
        # concatenates all the output files into a single file
        if edge_or_mid=='EDGE':
            # concatenate all files of the same side
            if var_input != 'uwnd':    
                os.system('cat ' + output_path  + label + '*_N.dat > ' + catfile_pre  + 'N.dat')
                os.system('cat ' + output_path  + label + '*_S.dat > ' + catfile_pre  + 'S.dat')
            if var_input != 'vwnd':
                os.system('cat ' + output_path  + label + '*_E.dat > ' + catfile_pre  + 'E.dat')
                os.system('cat ' + output_path  + label + '*_W.dat > ' + catfile_pre  + 'W.dat')
        elif edge_or_mid=='MID':  
            # concatenate all area averaged value files
            catfile = catfile_pre + 'mid.dat'
            cmd_cat = 'cat ' + output_path + label + '*_mid.dat > ' + catfile
            os.system(cmd_cat)
            
    # clear buffer contents
#     print ('clearing buffer ...')
#     clear_folder(output_path)
    
# ------------------- end year loop





print ('done')


