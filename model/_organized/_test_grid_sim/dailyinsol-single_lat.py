import yaml # pip install PyYAML==5.1.2
import numpy as np
from netCDF4 import Dataset
import os
from climlab import constants as const
from climlab.solar.insolation import daily_insolation
from climlab.solar.orbital import OrbitalTable
import sys

lat = int(sys.argv[1])

file_insol_single_lat = 'single_insol_' + str(lat) + 'N' + '.dat' 

insol = []
for day in range(0,365):
    insol.append(daily_insolation(lat, day) + 0)

with open(file_insol_single_lat, 'w+') as out_file:
    for i in range(len(insol)):
        out_string = "%.3f" % insol[i] + '\n'
        out_file.write(out_string)


print (file_insol_single_lat)
print ('done')

