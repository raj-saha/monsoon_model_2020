import yaml # pip install PyYAML==5.1.2
import numpy as np
from netCDF4 import Dataset
import os
from climlab import constants as const
from climlab.solar.insolation import daily_insolation
from climlab.solar.orbital import OrbitalTable

def read_controls(file_control):    
    with open('controls.yaml') as file:
        controls = yaml.load(file, Loader=yaml.FullLoader)
    return controls

def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
    # cmd_del_existing = 'rm ' + folder + '*' + var_input + '*.dat'
    # os.system(cmd_del_existing)


# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------

# # read yaml file
# controls              = read_controls('controls.yaml')

# dx = dy               = controls['dx']
# bounds_lat_S          = controls['bounds_lat_S']
# bounds_lat_N          = controls['bounds_lat_N']
# bounds_lon_W          = controls['bounds_lon_W']

# Ndy = int(np.floor((bounds_lat_N-bounds_lat_S)/dy))
# Ndx = Ndy

# bounds_lon_E = bounds_lon_W + Ndx * dx
# lats_arr = np.arange(bounds_lat_S, bounds_lat_N, dx)
# lons_arr = np.arange(bounds_lon_W, bounds_lon_E, dx)

# Ndy_str = '{0:02d}'.format(Ndy)
# Ndx_str = '{0:02d}'.format(Ndx)

# label_all = ["INAS", "INMID"]
# lat_N     = [29, 25]
# lat_S     = [25, 17]
# lon_W     = [89, 77]
# lon_E     = [93, 85]

label_all = ["05x05_05_05", "05x05_02_03"]
lat_N     = [10, 30]
lat_S     = [0,  20]
lon_W     = [50, 80]
lon_E     = [60, 90]


# ------------------------------------------------------------
# ------------------------------------------------------------


# output_folder_name = '_inputs_' + Ndy_str + 'x' + Ndx_str

# put in 0x_x_0x grid sizes in input folder name
# output_path_inputs = output_folder_name + '/_inputs/'
# create_folder(output_path_inputs)

print ('---------------------------------------')
print (' COMPUTE INSOLATION AT MIDDLE OF GRIDS  ')
print ('----------------------------------------')

for i_label in range(0, len(label_all)):
# for i_lat, val_lat in enumerate(lats_arr):
#     i_lat_str = '{0:02d}'.format(i_lat)

#     for i_lon, val_lon in enumerate(lons_arr):
#         i_lon_str = '{0:02d}'.format(i_lon)
#     print ('\nprocessing grid: ', i_lat_str, i_lon_str)
    label = label_all[i_label]
    output_folder_name = '_inputs/' + label
    print ('output folder:', output_folder_name)
    output_path_inputs = output_folder_name
        
    file_insol = output_path_inputs + '/input_insol.dat'
    print (file_insol)
    
    lat_mid = lat_S[i_label] + (lat_N[i_label] - lat_S[i_label])/2

    insol = []
    for day in range(0,365):
        insol.append(daily_insolation(lat_mid, day) + 0)

    with open(file_insol, 'w+') as out_file:
        for i in range(len(insol)):
            out_string = "%.3f" % insol[i] + '\n'
            out_file.write(out_string)



print ('done')

