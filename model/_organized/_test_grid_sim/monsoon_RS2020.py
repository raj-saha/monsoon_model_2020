# Functions written by Raj Saha, 2017
# Modified and simplified from Zickfeld 2010

# !! READS IN CONSTANT BOUNDARY CONDITIONS .. !!


import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import os
import sys 
from params_SENS import *
import csv
# import params_bif
# u_side_const = params_bif.u_side_const


# .......................................................................
# .........................................................BOX PARAMETERS
# ang_width_deg = 4

def box_params():

    ang_width_deg = 4

    width         = (ang_width_deg/360)*2 * np.pi * REarth  # width of box [m]
    A_g           = width**2
    m_g           = A_g*H_g*rho_g # mass of soil

    return width, A_g, m_g

# .......................................................................
# .........................................................TIME VARIABLES
def time_params(dur_sim_yr, dt_day):
    # dur_sim_yr = 1              # duration of simulation in years
    # dt_day     = 1                   # integration time-step in days

    sec_day    = 3600*24             # seconds in a day
    sec_month  = sec_day*30          # seconds in a year
    sec_year   = sec_day*365         # seconds in a year
    dt         = dt_day * sec_day    # integration time-step in sec
    dur_sim    = dur_sim_yr*sec_year # duration of simulation in sec

    dur_sim_months = dur_sim_yr*sec_year/sec_month
    
    return dur_sim, dur_sim_months, dt, sec_month


## get bifurcation parameter values passed on here
def get_bif_params(param_name, param_val):

    params_dict = {
					'u_side_const' : u_side_const,
					'T_side_const' : T_side_const,
					'q_side_const' : q_side_const,
					'insol_const'  : insol_const,

                    'f1' 		   : f1,
                    'k_Roff' 	   : k_Roff,
                    'k_e'          : k_e,
                    'tau_p'        : tau_p,

                    'a1st' : a1st,
                    'a2st' : a2st,
                    'a3st' : a3st,
                    'a1cu' : a1cu,
                    'a2cu' : a2cu
                }
    
    
    params_dict[param_name] = param_val

    return params_dict
    
# ----------------------------------------------------- READ BOUNDARY INPUTS
# ----------------------------------------------- Temp, Sp. Hum, Wind, insol
# ---------------------------------------------------- for a given box label
# --------------------------------------------------------------------------   

# REAL ANALYSIS BOUNDARY CONDITIONS AND TIME-VARYING INSOLATION
def read_inputs_RA(label):

    # reads RA data as boundary conditions
    input_path = '_inputs/' + label + '/input_'
    
    # reads idealized (spline interpolation) boundary conditions
    # input_path = '_inputs/' + label + '/ideal_' 

    input_file_T_N = input_path + 'T_N.dat'
    input_file_T_E = input_path + 'T_E.dat'
    input_file_T_S = input_path + 'T_S.dat'
    input_file_T_W = input_path + 'T_W.dat'

    input_file_q_N = input_path + 'q_N.dat'
    input_file_q_E = input_path + 'q_E.dat'
    input_file_q_S = input_path + 'q_S.dat'
    input_file_q_W = input_path + 'q_W.dat'

    input_file_u_N = input_path + 'u_N.dat'
    input_file_u_E = input_path + 'u_E.dat'
    input_file_u_S = input_path + 'u_S.dat'
    input_file_u_W = input_path + 'u_W.dat'


    # get number of lines in one file
    # (they should all be the same)
    Ndays = 0
    with open(input_file_T_N, 'r') as f:
        for line in f:
            Ndays += 1

    T_side = np.zeros((Ndays, 4))
    q_side = np.zeros((Ndays, 4))
    u_side = np.zeros((Ndays, 4))

    T_side[:,0] = np.loadtxt(fname = input_file_T_N)
    T_side[:,1] = np.loadtxt(fname = input_file_T_E)
    T_side[:,2] = np.loadtxt(fname = input_file_T_S)
    T_side[:,3] = np.loadtxt(fname = input_file_T_W)

    q_side[:,0] = np.loadtxt(fname = input_file_q_N)
    q_side[:,1] = np.loadtxt(fname = input_file_q_E)
    q_side[:,2] = np.loadtxt(fname = input_file_q_S)
    q_side[:,3] = np.loadtxt(fname = input_file_q_W)

    u_side[:,0] = np.loadtxt(fname = input_file_u_N)
    u_side[:,1] = np.loadtxt(fname = input_file_u_E)
    u_side[:,2] = np.loadtxt(fname = input_file_u_S)
    u_side[:,3] = np.loadtxt(fname = input_file_u_W)
    
    # Flip sign of N and E Wind inputs    
    u_side[:,0] = -u_side[:,0]
    u_side[:,1] = -u_side[:,1]
    
    # read insoaltion
    # should be specific to the center of each box. 
    # currently there is only one insolation input file
    input_file_insol = '_inputs/' + label + '/input_' + 'insol.dat'

    # insol = np.zeros((1827, 1))
    insol = np.loadtxt(fname = input_file_insol)
    
    return T_side, q_side, u_side, insol
# --------------------------------------------------------------------------   
# --------------------------------------------------------------------------   


# CONSTANT BOUNDARY CONDITIONS AND INSOLATION
def read_inputs_const_bcs(dur_sim_yr, bif_param, bif_val):
    
    params_dict   = get_bif_params(bif_param, bif_val)
    
    u_side_const  = params_dict['u_side_const']
    T_side_const  = params_dict['T_side_const']
    q_side_const  = params_dict['q_side_const']
    insol_const   = params_dict['insol_const']
    
    # keep array structures the same, 
    # just read in the constant values

    # get number of lines in one file  (they should all be the same)
    Ndays = int(np.ceil(dur_sim_yr * 365))

    T_side  = np.zeros((Ndays, 4))
    q_side = np.zeros((Ndays, 4))
    u_side  = np.zeros((Ndays, 4))

    T_side[:,0] = T_side_const
    T_side[:,1] = T_side_const
    T_side[:,2] = T_side_const
    T_side[:,3] = T_side_const

    q_side[:,0] = q_side_const
    q_side[:,1] = q_side_const
    q_side[:,2] = q_side_const
    q_side[:,3] = q_side_const

    u_side[:,0] = u_side_const
    u_side[:,1] = u_side_const
    u_side[:,2] = u_side_const
    u_side[:,3] = u_side_const

    # Flip sign of N and E Wind inputs
    # to account for sign convention and flux into the box
    ## should not apply if wind is all incoming or all outgoing from all sides.
    # u_side[:,0] = -u_side[:,0]
    # u_side[:,1] = -u_side[:,1]
    
    # read insoaltion
    insol = insol_const
    
    return T_side, q_side, u_side, insol
# --------------------------------------------------------------------------   





# -------------------------------------- Temperature as a function of height
def Tz(z, Ta, qa, Ncu):
    return (Ta - (gamma0 + gamma1*(Ta-T0)*(1-aq*qa**2) - gamma2*Ncu)*z)

# -------------------------------- Specific humidity as a function of height
def qz(z, qa):
    return (qa*np.exp(-z/He))

# ------------------------------------------ Density as a function of height
def rho(z):
    return (rho0*np.exp(-z/H0))

# ----------------------------------------- Pressure as a function of height
def p(z):
    return (p0*np.exp(-z/H0))

# ------------------------------------------------------ saturation humidity
def qsat(z, Ta, qa, Ncu):
    return (qsat0*(p0/p(z))*np.exp(-((0.622*L/R)*(1/Tz(z,Ta,qa,Ncu)-1/T0))))

# -------------------------------- RUN TIME FUNCTIONS, COMPUTED FOR EACH DAY
# ---------------------------------------------- vertical wind speed, u_vert
def u_vert(t_day, A_g, width, T_side, q_side, u_side):    
    return width * integrate.quad(rho,0,H_mon)[0] * np.sum(u_side[t_day,:])/(A_g*rho(np.mean([H_Had, H_mon])))

def adv_heat_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu):
    return (Cair * width * ( \
                            + integrate.quad(lambda z: u0_p[i]*rho(z)*Tz(z, T_side[t_day,i], qa, Ncu), 0, H_mon  )[0] \
                            - integrate.quad(lambda z: u1_p[i]*rho(z)*Tz(z, Ta, qa, Ncu), H_mon, H_trop)[0] \
                            - integrate.quad(lambda z: u0_m[i]*rho(z)*Tz(z, Ta, qa, Ncu), 0, H_mon  )[0] \
                            + integrate.quad(lambda z: u1_m[i]*rho(z)*Tz(z, T_side[t_day,i], qa, Ncu), H_mon, H_trop)[0] \
                          ))

# ------------------------------------------------------- MOISTURE ADVECTION
# --------------------------------------------------------------------------
def adv_q_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu):    
    return (width * ( \
                            + integrate.quad(lambda z: u0_p[i]*rho(z)*qz(z, q_side[t_day,i]), 0, H_mon  )[0] \
                            - integrate.quad(lambda z: u1_p[i]*rho(z)*qz(z, qa), H_mon, H_trop)[0] \
                            - integrate.quad(lambda z: u0_m[i]*rho(z)*qz(z, qa), 0, H_mon  )[0] \
                            + integrate.quad(lambda z: u1_m[i]*rho(z)*qz(z, q_side[t_day,i]), H_mon, H_trop)[0] \
                          ))

# ------------------------------------------------------------- CLOUD SCHEME
# --------------------------------------------------------------------------
def clouds(v, qa, Ta, a1st, a2st, a3st, a1cu, a2cu):
    # ------------------------------------------------------ cumulus clouds
    if (v>0):    # upward velocity
        Ncu = Ncu0 * np.tanh(v/a1cu) * np.tanh(qa/a2cu)
    else: # downward velocity
        Ncu = 0 

    # ------------------------------------------------------ stratus clouds
    Nst   = (qa/qsat(0,Ta,qa,Ncu))**(1.5) * (a1st+a2st*(1+np.tanh(v/a3st)))

    # --------------------------------------------------------- total cloud
    N    = 1 - (1-Ncu)*(1-Nst)

    # --------------------------------------------------------- clear sky
    Ncs  = 1 - N
    
    return Ncu, Nst, N, Ncs

# ------------------------------------------- EULER'S SCHEME TO SOLVE SYSTEM
# --------------------------------------------------------------------------
def solve_monsoon_ODEs_euler(**controls_single_run):


    dur_sim_yr       = controls_single_run['dur_sim_yr']
    dt_day           = controls_single_run['dt_day']
    veg_albedo       = controls_single_run['veg_albedo']      
    veg_evap         = controls_single_run['veg_evap']     

    dyn_Ta           = controls_single_run['dyn_Ta']
    dyn_qa           = controls_single_run['dyn_qa']
    dyn_v            = controls_single_run['dyn_v']

    bc_type          = controls_single_run['bc_type']       
    bc_region        = controls_single_run['bc_region']     

    param_bif        = controls_single_run['param_bif']
    param_val        = controls_single_run['param_val']
    
    label = bc_region
    
    # READ INITIAL CONDITIONS
    Ta = controls_single_run['initial_conditions'][0]
    qa = controls_single_run['initial_conditions'][1]
    w  = controls_single_run['initial_conditions'][2]
    w2 = controls_single_run['initial_conditions'][3]
    
    # SET TUNABLE PARAMETER VALUES TO PASSED VALUES
    params_dict   = get_bif_params(param_bif, param_val)    
    a1st    = params_dict['a1st']
    a2st    = params_dict['a2st']
    a3st    = params_dict['a3st']
    a1cu    = params_dict['a1cu']
    a2cu    = params_dict['a2cu']
    f1      = params_dict['f1']
    k_Roff  = params_dict['k_Roff'] 
    k_e     = params_dict['k_e']
    tau_p   = params_dict['tau_p']
    
    
    # GET TIME VARIABLES
    dur_sim, dur_sim_months, dt, sec_month   = time_params(dur_sim_yr, dt_day)    
    
    # GET BOX PARAMETERS
    width, A_g, m_g = box_params()
    

    # GET BOUNDARY CONDITIONS AND INSOLATION
    # ......................................
    if bc_type == 'RA':
        # ............................. RA BOUNDARY CONDITIONS AND TIME-VARIABLE INSOL
        T_side, q_side, u_side, insol = read_inputs_RA(label)
        
    elif bc_type == 'FIXED':
        # ............................. FIXED BOUNDARY CONDITIONS AND INSOLATION
        T_side, q_side, u_side, insol = read_inputs_const_bcs(dur_sim_yr, param_bif, param_val)
        

    # get observed data    
    obs_temp_RA, obs_shum_RA, obs_v_mid_RA =  get_obs_vars(bc_region)    


    
    print ('simulation length = ', dur_sim_yr, ' yr')
    print ('time step = ', dt_day, ' days\n')
    print ('region: ', label ,'. bc type:', bc_type)
    print ('bifurcation param:', param_bif , '=', param_val)
    print ('-------------------------------')
    

    # initialize arrays
    Ta_arr       = []
    qa_arr       = []
    w_arr        = []
    P_mmpday_arr = []
    E_mmpday_arr = []
    Ncu_arr      = []
    v_arr        = []
    adv_heat_arr = []

    ## //////////////////////////////////////////////////////////// TIME LOOP
    ## //////////////////////////////////////////////////////////////////////
    t = dt
    
    while (t<dur_sim):
        # current time
        t_day         = int(np.floor(t/sec_day)-1)
        t_months      = int(np.floor(t/sec_month))
        t_day_of_year = int(np.remainder(np.ceil(t/sec_day),365)-1)

        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< READ INPUT VARIABLES 
        # INPUTS: Temperature, Sp Hum, Wind, and Insolation

#         # Wind inputs    
#         u_side[t_day,0] = -u_side[t_day,0]
#         u_side[t_day,1] = -u_side[t_day,1]

        ## DYNAMIC WIND - AMIT'S PRESCRIPTION
        # if switch_dyn_u == 'True':
        #     u_side[t_day, :] = (sin_alpha0*g*p00*H_mon/(ro00*f*2*R*T00*T00*width/100))*(Ta-T_side[t_day, :])


        # Insolation input [W m^-2]
        Iin           = insol[t_day_of_year]

        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        # --------------------------------------------------- vertical velocity 
        if dyn_v == False:
            v = obs_v_mid_RA[t_day]
        else:
            v = u_vert(t_day, A_g, width, T_side, q_side, u_side)

        # --------------------------------------------------- clouds
        Ncu, Nst, N, Ncs = clouds(v, qa, Ta, a1st, a2st, a3st, a1cu, a2cu)

        # Ncudot
        if (t<=dt):
            Ncudot = 0
        else:
            Ncudot = -(Ncu_old - Ncu)/dt


        # -------------------------------------------------------------- albedo
        # (clouds have specified albedos)
        alpha = Nst*alphast + Ncu*alphacu + Ncs*alphacs


        # ======================================================= NET WIND FLUX
        # (height change of N side due to Tibet not included)
        # ------------------------------------------------------------- monsoon 
        # (positive values imply box-ward flow)
        # u0 := surface wind
        # u1 := top wind

        # UP/DOWN-STREAM flows
        u0_p = (1/2)*(abs(u_side[t_day,:]) + u_side[t_day,:])
        u0_m = (1/2)*(abs(u_side[t_day,:]) - u_side[t_day,:])
        u1_p = k_u_mon * u0_p
        u1_m = k_u_mon * u0_m
        
        # heat advection, summed from all sides
        adv_heat = sum(adv_heat_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu) for i in range(0, 4)) 

        # moisture advection, summed from all sides
        adv_q    = sum(adv_q_side(i, t_day, width, Ta, qa, T_side, q_side, u_side, u0_p, u1_p, u0_m, u1_m, Ncu) for i in range(0, 4)) 


        # ================================================= CONDENSATION HEIGHT
        # From the intersection of dew point and dry lapse rates
        # qaREL = 0.263*p0*qa*(exp(17.67*(Ta-T0)/(Ta-29.65)))^(-1)

        qaREL = qa/qsat(0, Ta, qa, Ncu)
        Tadew = Ta - (100-qaREL)/5

        H_c   = max((Ta-Tadew)/(Gamma_a - Gamma_dew),0)

        # ======================================================= PRECIPITATION
        if (H_c<H_trop and H_c>=0):
            # Zickfeld
            P = max((N/tau_p) * integrate.quad(lambda z: (rho(z)*qz(z,qa)), H_c, H_trop)[0], 0)
            #      P = max((N/tau_p) * quad(@(z) (rho(z).*qz(z,qa)), 0, H_trop),0)
        else:
            P = 0

        # ========================================================= EVAPORATION
        # (from ground only, only positive)
        # total surface wind:
        # usfc = abs(sum(u_side))

        usfc = np.sqrt((u_side[t_day,:][0]+u_side[t_day,:][2])**2 + (u_side[t_day,:][1]+u_side[t_day,:][3])**2)

        E  = max(k_e * w * usfc * rho0 * (qsat(0,Ta,qa,Ncu)-qa), 0)

        # =========================================================== RADIATION
        Iout  = B0 + B1*(Ta-T0) - N*(B2 + B3*(Ta-T0))

        # ================================================= GOVERNING EQUATIONS
        # MOISTURE EQ
        qdot_L      = (E-P)/(((1-np.exp((-2*H_trop)/H0))*H0*rho0)/2)
        qdot_adv    = (adv_q/A_g)/(((1-np.exp((-2*H_trop)/H0))*H0*rho0)/2)

        qadot       = qdot_L + qdot_adv

        # HEAT EQ
        RHS_HEATEQ  = A_g * (Iin*(1 - alpha) - Iout + L*(P - E)) + adv_heat

        Tadot       = (Cg*np.exp(1)**(H0**(-1)*H_trop)*m_g+A_g*Cair*H0*((-1)+(-1)* \
            gamma1*(H0+H_trop)*((-1)+aq*qa**2)+np.exp(1)**(H0**(-1)*H_trop)*( \
            1+gamma1*H0*((-1)+aq*qa**2)))*rho0)**(-1)*(np.exp(1)**(H0**(-1) \
            *H_trop)*RHS_HEATEQ+(-1)*A_g*Cair*H0*(((-1)+np.exp(1)**(H0**(-1)*H_trop) \
            )*H0+(-1)*H_trop)*rho0*(gamma2*Ncudot+2*aq*gamma1*qa*qadot* \
            ((-1)*T0+Ta)))

        # GROUND WATER EQ
        Roff  = k_Roff * P * w

        # wdot  = ((P - E - Roff)/f1) - (w-w2)/tau_w
        wdot  = ((P - E - Roff)/f1)
        # w2dot = (f1/f2) * (w-w2)/tau_w
        w2dot = 0

        # put these in a function and return some diagnostic variables.
        # =================================================== EULER INTEGRATION
        Ta = Ta + Tadot*dt
        qa = min(max(qa + qadot*dt, 1e-10),1)
        w  = min(max(w + wdot*dt, 0),1)
        w2 = min(max(w2 + w2dot*dt,0),1)
        
        # replace Ta and qa with observed data instead of dynamic
        # put in a control condition here (or above)
        if dyn_Ta == False:
            Ta = obs_temp_RA[t_day]
        if dyn_qa == False:
            qa = obs_shum_RA[t_day]

        # ============================================================== OUTPUT
        if (np.remainder(t, sec_day)==0): ## write values once every day      
            P_mmpday = P * sec_day
            E_mmpday = E * sec_day

            P_mmpday_arr.append(P_mmpday)
            E_mmpday_arr.append(E_mmpday)
            Ta_arr.append(Ta)
            qa_arr.append(qa)
            w_arr.append(w)
            Ncu_arr.append(Ncu)
            v_arr.append(v)
            adv_heat_arr.append(adv_heat)

            # 1    2   3   4  5  6    7         8         9                     10 11
            # t_day, Ta, qa, w, v, H_c, E_mmpday, P_mmpday, qsat(0,Ta,qa,Ncu)-qa, N, Ncu)

        
        if (np.remainder(t,sec_month)==0):
            print ('\r', 'month: ', int(t/sec_month), '/', int(dur_sim_months), end='')

        # ====================================================== TIME INCREMENT
        t = t + dt
        Ncu_old = Ncu
    ## //////////////////////////////////////////////////////// END TIME LOOP
    ## //////////////////////////////////////////////////////////////////////

    Ta_arr       = np.array(Ta_arr)
    qa_arr       = np.array(qa_arr)
    w_arr        = np.array(w_arr)
    P_mmpday_arr = np.array(P_mmpday_arr)
    E_mmpday_arr = np.array(E_mmpday_arr)
    Ncu_arr      = np.array(Ncu_arr)
    v_arr        = np.array(v_arr)
    adv_heat_arr = np.array(adv_heat_arr)

    return Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr, T_side, q_side, u_side, params_dict
# ...........................................................................................
# ...........................................................................................

def get_obs_vars(region):

    # get RA temp, shum, and vertical velocity for box average
    
    file_temp_RA = '_inputs/' + region + '/input_T_mid.dat'
    data_temp_RA = np.loadtxt(file_temp_RA, skiprows=0)    

    file_shum_RA = '_inputs/' + region + '/input_q_mid.dat'
    data_shum_RA = np.loadtxt(file_shum_RA, skiprows=0)  
    
    file_omega_RA = '_inputs/' + region + '/input_v_mid.dat'
    data_omega_RA = np.loadtxt(file_omega_RA, skiprows=0)
    data_v_mid_RA = -data_omega_RA/(rho(H_mon)*g)
    
    return data_temp_RA, data_shum_RA, data_v_mid_RA

# ---------------------------------------------------- PLOTTING ROUTINE
def plot_arrays(Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr, T_side, q_side, u_side, label, plotfile, output_plot_title, out_plots_single_show):

    region = label.split('_')[0]
    
    # get observed temperature, shum, and vertical velocity
    obs_temp_RA, obs_shum_RA, obs_v_mid_RA =  get_obs_vars(region)
    
    import seaborn as sns
    sns.set(style="darkgrid")

    fig, axs = plt.subplots(4,2, figsize = (18,10))
    
    # plot observed temp
    axs[0, 0].step(obs_temp_RA[1:len(Ta_arr)], 'red', alpha=1, linewidth=1.2)
    
    axs[0, 0].step(Ta_arr, 'k')
    axs[0, 0].set_title('Temperature')
    axs[0, 0].set(ylabel = 'K')

    # plot observed shum  
    axs[0, 1].step(obs_shum_RA[1:len(Ta_arr)], 'red', alpha=1, linewidth=1.2)
    
    axs[0, 1].step(qa_arr, 'purple')
    axs[0, 1].set_title('Specific Humidity')
    axs[0, 1].set(ylabel = '')
    
    axs[1, 0].step(w_arr, 'g')
    axs[1, 0].set_title('Ground Moisture')
    axs[1, 0].set(ylabel = '')

    # get observed PPT and plot it
    file_TRMM = '_obs/' + region + '/ppt_TRMM_box/PPT_2000-2004.dat'
    obs_TRMM = np.loadtxt(file_TRMM, skiprows=0)
    axs[1, 1].step(obs_TRMM[1:len(Ta_arr)], 'red', alpha=1, linewidth=1)
    
    axs[1, 1].step(P_mmpday_arr[1:], '-', linewidth=1)
    axs[1, 1].set_title('Precipitation ')
    axs[1, 1].set(ylabel = '[mm/day]')

    axs[2, 0].step(E_mmpday_arr[1:], '-', linewidth=1)
    axs[2, 0].set_title('Evaporation ')
    axs[2, 0].set(ylabel = '[mm/day]')
    
    axs[2, 1].step(Ncu_arr[1:], '-', linewidth=1)
    axs[2, 1].set_title('Cumulous Clouds ')
    axs[2, 1].set(ylabel = '[1]')
    
    # plot observed vertical velocity
    axs[3, 0].step(obs_v_mid_RA[2:len(Ta_arr)], 'red', alpha=1, linewidth=1.2)
    
    axs[3, 0].step(v_arr[1:], '-', linewidth=1)
    axs[3, 0].set_title('Vertical Velocity ')
    axs[3, 0].set(ylabel = '[m/s]')
    
    axs[3, 1].step(adv_heat_arr[1:], '-', linewidth=1)
    axs[3, 1].set_title('Advected Heat ')
    axs[3, 1].set(ylabel = '[]')

    for ax in axs.flat:
        ax.set(xlabel='Days')

    fig.tight_layout(pad=3.0)

    fig_title = 'Model Outputs [' + output_plot_title + ']'
    fig.suptitle(fig_title, fontsize=16)
    
    # # Output 
    output_folder = '/'.join(plotfile.split('/')[:-1])
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        
    print ('\nPlot file saved as', plotfile)

    if plotfile != None:
        plt.savefig(plotfile)  
    
#     plt.show()

    plt.close(fig)

# ---------------------------------------------------- SAVE ARRAYS 
def save_arrays_params(Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr, output_folder, output_file_data_prefix, params):
    
    output_folder = output_folder + '/data/'
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        
    output_array_combined = np.array([Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, Ncu_arr, adv_heat_arr, v_arr])
    output_array_combined = output_array_combined.T

    output_file_temp   = output_file_data_prefix + 'temp.dat'
    output_file_shum   = output_file_data_prefix + 'shum.dat'
    output_file_w      = output_file_data_prefix + 'w.dat'
    output_file_ppt    = output_file_data_prefix + 'ppt.dat'
    output_file_evap   = output_file_data_prefix + 'evap.dat'
    output_file_Ncu    = output_file_data_prefix + 'Ncu.dat'
    output_file_advQ   = output_file_data_prefix + 'advQ.dat'
    
    np.savetxt(output_file_temp, Ta_arr)
    np.savetxt(output_file_shum, qa_arr)
    np.savetxt(output_file_w, w_arr)
    np.savetxt(output_file_ppt, P_mmpday_arr)
    np.savetxt(output_file_evap, E_mmpday_arr)
    np.savetxt(output_file_Ncu, Ncu_arr)
    np.savetxt(output_file_advQ, adv_heat_arr)
    
    # write parameter values into a single file
    output_file_params = output_file_data_prefix + 'params.csv'

    with open(output_file_params, 'w') as csv_file:  
        writer = csv.writer(csv_file)
        for key, value in params.items():
            writer.writerow([key, value])
    

    print ('\n\noutput folder: ', output_folder)
    
    
