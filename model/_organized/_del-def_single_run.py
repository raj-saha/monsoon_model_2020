# MAIN SCRIPT TO RUN MONSOON SIMULATION
# Raj Saha, 2020
# organized build

# read controls

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import sys
import time
import os

from params import *
import monsoon_RS2020 as monRS20

    
def single_run(**controls):

#     doc = 'test.yaml'
#     print yaml.dump(yaml.load(doc))

#     read_controls(**controls)
    
    dur_sim_yr            = controls['dur_sim_yr']
    dt_day                = controls['dt_day']
   
    # single parameter name
    bif_param             = controls['ctl_run']['bif_params'][0][0]

    veg_albedo            = controls['ctl_run']['veg_albedo']
    veg_evap              = controls['ctl_run']['veg_evap']
    
    dynamic_Ta            = controls['ctl_run']['dynamic_Ta']
    dynamic_qa            = controls['ctl_run']['dynamic_qa']
    dynamic_v             = controls['ctl_run']['dynamic_v']
       
    bc_type               = controls['ctl_bcs']['type']
    bc_region             = controls['ctl_bcs']['region']
    
    out_data_all_sims     = controls['ctl_out']['data_all_sims']
    out_data_eqpts        = controls['ctl_out']['data_eqpts']
    out_plots_all_sims    = controls['ctl_out']['plots_all_sims']
    out_plots_eqpts       = controls['ctl_out']['plots_eqpts']
    out_plots_single_show = controls['ctl_out']['plots_single_show']

#     f1  = controls['f1']
    
    # .......................................................................
    # SET TIME PARAMETERS
    # .......................................................................
#     dur_sim_yr = 2              # duration of simulation in years
#     dt_day     = 1              # integration time-step in days


    param_val = 321
    param_bif = bif_param


    # .......................................................................
    ## DEFINE INITIAL CONDITIONS
    # .......................................................................
    Ta0 = Ts
    qa0 = 0.015
    w0  = 0.35
    w20 = 0


    Ta = Ts
    qa = qa0
    w  = w0        # soil moisture content [kg m^-2]
    w2 = w20           # soil moisture content [kg m^-2]

    label = bc_region
    # .......................................................................
    # CALL SYSTEM SOLVER
    # .......................................................................
    Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, T_side, q_side, u_side = monRS20.solve_monsoon_ODEs_euler(dur_sim_yr, dt_day, Ta, qa, w, w2, param_bif, param_val, label)


    return Ta_arr, qa_arr, w_arr, P_mmpday_arr, E_mmpday_arr, T_side, q_side, u_side




